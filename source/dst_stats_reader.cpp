#include <hloop.h>

#include <TString.h>

#include <getopt.h>

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

int main(int argc, char** argv)
{
    int verbose = 0;
    std::string label;
    std::string outfile;

    while (1)
    {
        static struct option long_options[] = {/* These options set a flag. */
                                               {"verbose", no_argument, &verbose, 1},
                                               {"brief", no_argument, &verbose, 0},

                                               /* These options don’t set a flag.
                                                * We distinguish them by their indices. */
                                               {"label", required_argument, 0, 'l'},
                                               {"output", required_argument, 0, 'o'},
                                               {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        int c = getopt_long(argc, argv, "l:o:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0) break;
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'l':
                label = optarg;
                break;

            case 'o':
                outfile = optarg;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    /* Instead of reporting ‘--verbose’
     *      and ‘--brief’ as they are encountered,
     *           we report the final status resulting from them. */
    if (verbose) puts("verbose flag is set");

    HLoop* loop = new HLoop(kTRUE);

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        Bool_t ret;
        // 		printf ("non-option ARGV-elements: ");
        while (optind < argc)
        {
            const TString infile = argv[optind++];
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
                ret = loop->addFiles(infile);
            else
                ret = loop->addFilesList(infile);

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    if (!loop->setInput("-*,+HParticleCand,+HForwardCand"))
    { // reading file structure
        std::cerr << "READBACK: ERROR : cannot read input !\n";
        std::exit(EXIT_FAILURE);
    }

    std::stringstream ss;
    ss << loop->getEntries();
    std::cout << ss.str() << "\n";

    if (outfile.length())
    {
        auto out_file = std::ofstream(outfile.c_str());
        out_file << ss.str();
    }

    exit(0);
}

install(
    TARGETS
        hades_elastics_finder_exe
        dst_stats_reader_exe
    RUNTIME COMPONENT hades_elastics_lumi_Runtime
)

install(
    PROGRAMS
        scripts/batch_script_dst_stats.sh
        scripts/batch_script_lumi_slices.sh
        scripts/hades_lumi_merger.py
        scripts/hades_lumi_plot.gp
        scripts/hades_lumi_plot.sh
        ${forward_tools_SOURCE_DIR}/online_tools/online_helpers.py
        scripts/make_dst_lists.sh
        scripts/make_lumi_stats.sh
        scripts/make_slice_lists.sh
        scripts/submit_dst_stats.sh
        scripts/submit_lumi_slices.sh
    DESTINATION ${CMAKE_INSTALL_BINDIR}
)

install(
    FILES
        scripts/hadeslogo-big.png
        ${CMAKE_BINARY_DIR}/scripts/config.sh.example
    DESTINATION ${CMAKE_INSTALL_BINDIR}
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()

# Building with CMake

## Build

This project doesn't require any special command-line flags to build to keep
things simple.

Here are the steps for building in release mode with a single-configuration
generator, like the Unix Makefiles one:

```sh
cmake -S . -B build -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=$DESTDIR
cmake --build build
```

The `CMAKE_INSTALL_PREFIX` flag provides the install location.
See [README](README.md) for details.

Be aware that `vae23` provides `cmake-3.13.4` which is too old. This project
requires at least `3.14`. One can check the system's `cmake` current version
with `cmake -version` and if it is lower than `3.14` then please use user
provided version of cmake available at `/u/rlalik/.local/10/bin/cmake` (works
on `vae23` and `debian10` container). If cmake version is at least `3.14` one
can freely use system provided `cmake`.

To allow to reuse the same sources for possible future builds with different
`vae` systems and `hydra` releases, one can use specific build directory names,
e.g. for `hydra-6.7a` compield at `vae23` the `build` directory in the examples
can be replaced with `build_vae23_67a`.

In that case also install location could be customized, e.g.
`/lustre/hades/user/${USER}/debian10/feb22/gen3/` says that this is `debian10`
(`vae23`) compatible for Feb22 gen3 release, however there is no dependency on
hydra release. Unless specifically required, `hydra`-version dependency for
install locations is not really needed.


## Install

This project doesn't require any special command-line flags to install to keep
things simple. As a prerequisite, the project has to be built with the above
commands already.

The below commands require at least CMake 3.15 to run, because that is the
version in which [Install a Project][1] was added.

Here is the command for installing the release mode artifacts with a
single-configuration generator, like the Unix Makefiles one:

```sh
cmake --install build
```

With `cmake` older than `3.15.0` one needs to use `make` for installation:

```sh
cd build
make install
```

[2]: https://cmake.org/cmake/help/latest/manual/cmake.1.html#install-a-project

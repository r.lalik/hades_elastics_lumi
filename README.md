# hades_elastics_lumi

This is the hades_elastics_lumi project.

This software tools allows to measure various kinds of luminosity using the measured elastics scattering events.

The luminosity could be determined as the total accumulated over the whole beam time. In that case one need to find all elastics events in the beam time period and calculate the luminosity. However, if one wants to find also the trend over time, one need to calculate luminosity in some intervals.

Using time intervals is not always possible as the beam intensity varies and in certain interval there may be none or very little elastic events. In this tool we use the events intervals approach. It was found out that bunch of 10 million events is sufficient to collect enough elastic events to perform calculations.

For a such event intervals approach, the input data must be sorted, as the each interval is associated with time. The HADES event builder (EB) produces N files in parallel, stored in separate directories for each event builder, as here: `/lustre/hades/dst/feb22/gen3/{01..10}/`. Thus one cannot simply sort the file list by alphabetical order as the all event builder 01 files will be before 02 and so on. One need to sort them by the timestamp regardless the event build, which will be the secondary sorting key.

Next, as the events interval will be often appearing inside the files, there will always be events mixing around intervals edges. Some events which should belong to given interval will be assigned to the following interval. How ever these errors are not significant and can be ignored.

It is not a single tool but rather collection of tools required to be run in certain order. Due to resources available for HADES experimenters in the GSI computing network, it is intended to be run in two environments: the computing farm and the local os. Instructions below describe the whole procedure.

## Prerequisites

Tool was used and tested with following software:

* `hydra-6.7a`
* `root-6.24.02`
* `>=cmake-3.14.0`
* `>=gnuplot-5.4.8` (only on local OS, see Usage section)

It is intended to be used on the `vae23` computing nodes with `debian10` container. In the future newer version of `vae` servers can be used if appropriate HADES toolset (`hydra`) is also available.

# Building and installing

See the [BUILDING](BUILDING.md) document.

# Usage

Usage is divided into two steps, first on one the computing farm and second locally. Moreover, to obey requirements regarding to usage of lustre disks, we will discuss three location:
1. source files location denoted as `$SOURCES` in user's home directory, e.g. `~/hades/software/src/`,
1. lustre/farm location denoted as `$DESTDIR`, e.g. `/lustre/hades/user/${USER}/debian10/feb22/gen3/lumi` (example for `feb22` beam time and `gen3` dst production),
1. local PC/Linux location (neither GSI or Lustre) at `$LOCAL`.

The local PC is required to run software not available in GSI's Debian containers.

## Sources

Before building, obtain the sources with following commands:

```sh
cd $SOURCES
git clone https://git.gsi.de/r.lalik/hades_elastics_lumi.git
cd hades_elastics_lumi
```

Before building, one has to load appropriate hydra profile as the `$HADDIR` must be exported on the configuration step.

Please see the [BUILDING](BUILDING.md) for general instructions how to compile the project.

## Elastics computing

Here we first prepare the lustre files, and then run the computing jobs to calculate number of elastics. Next, one hast to prepare slice files and run elastics calculations in each slice. In the final step, one has to merge all slices to create the elastics integral trend data. These data can be used to calculate luminosity and create plots.

1. After installing the files on lustre in the `$DESTDIR` location, one can find `$DESTDIR/bin/` directory which contains all necessary programs and scripts.

   There is also a file called `config.sh.example` which contains example of configuration for scripts. This file can be used to create the proper configuration:

    ```sh
    cd $DESTDIR/bin
    cp config.sh.example config.sh
    ```

   With this approach, reinstalling the project (e.g. some bugfixes) will not overwrite the `config.sh`.
   If there are some future changes in `config.sh.example` however, they will need to be manually merged into config. User must verify this after new installation, e.g. using `diff`.

   The provided default values for variables are in general sufficient, however one or two variables need manual configuration. Those are:
   * `HYDRA_PROFILE` - profile file location
   * `DST_FILES_DIR` - directory containing dst files, e.g. `/lustre/hades/dst/feb22/gen3`

   If cmake could find `defall.sh` fine in `$HADDIR` location on configuration step, the `HYDRA_PROFILE` will be already set and do not need further actions unless the profile will need to be changed.

   Edit `config.sh` file and set these variables. For example, the config may look like:

    ```sh
    HYDRA_PROFILE=/cvmfs/hadessoft.gsi.de/install/debian10/6.24.02/hydra2-6.7a/defall.sh
    DST_FILES_DIR=/lustre/hades/dst/feb22/gen3 # set path to dst files
    ```

   There is no need to change other settings. Worth remembering is `$DATA_DIR` which by defaults points to `../data`.

   You can source the config file to use the variables from inside:

    ```bash
    . config
    ```

1. Create dst lists for each beam day. The list must be sorted by the filename. The script takes the list of the beam days to be proceeded, e.g. for feb22 it is

    ```sh
    ./make_dst_lists.sh {032..053} {059..068}
    ```

    The output will be stored in the`$DATA_DIR/$DST_LISTS_OUT_DIR` with one file per day. Please verify that the files are properly sorted, it should look line:

        /lustre/hades/dst/feb22/gen3/032/01/root/be2203221095901.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/02/root/be2203221095902.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/03/root/be2203221095903.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/04/root/be2203221095904.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/05/root/be2203221095905.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/06/root/be2203221095906.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/07/root/be2203221095907.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/08/root/be2203221095908.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/09/root/be2203221095909.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/10/root/be2203221095910.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/01/root/be2203221134201.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/02/root/be2203221134202.hld_dst_feb22.root

    Note that first we have all `be22032210959xx` files, then `be22032211342xx` and so on. This is not standard alphabetic sorting as the event builder directories are ignored. Otherwise we would have:

        /lustre/hades/dst/feb22/gen3/032/01/root/be2203221095901.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/01/root/be2203221134201.hld_dst_feb22.root

    before

        /lustre/hades/dst/feb22/gen3/032/02/root/be2203221095902.hld_dst_feb22.root
        /lustre/hades/dst/feb22/gen3/032/02/root/be2203221134202.hld_dst_feb22.root

    and so on.

1. Create dst statistics files. At the moment our statistics consint only of number of events in a day files. This is required to calculate number of slices per day.

    For that, call the `submit_dst_stats.sh` macro with list of days to be analyzed, e.g. for feb22:

    ```sh
    ./submit_dst_stats.sh {032..053} {059..068}
    ```

    This will execute jobs on the farm, you need to wait until they are done. The output will be stored in `$DATA_DIR/$DST_STATS_OUT_DIR`.

1. Create slice files. Slices are used to calculate number of elastics in events intervals. They cannot be to large and to small.
   For feb22 the optimal value is in range of few million events. This example makes slices of 10M events.

    ```sh
    ./make_slice_lists.sh 10000000 {032..053} {059..068}
    ```

   The first argument must always be number of events. The output will be stored in `$DATA_DIR/$LUMI_DIR/XXX/$LUMI_SLICES_DIR` where `XXX` is a human readable form of slice size.
   In the example case, it will be `../data/lumi/10M/slices`.

   The scripts uses `numfmt -to=si [value]` command to convert the number into SI-unit suffixes, but it is insensitive to small value variations, e.g. `10000000` and `10000100` will both results in the same label `10M`. be aware of this when creating several sets of slices. You can always test the expected output with `numfmt`.

   The slice file has form of (example):

   ```text
   032 46428686 10000000
   ```

   where the first value is the day number, the second is the total number of events in that day and the third one is the slcie size.

1. Run slices analysis with arguments being the slice files, e.g.

    ```bash
    ./submit_lumi_slices.sh $DATA_DIR/$LUMI_DIR/10M/$LUMI_SLICES_DIR/*.txt
    ```

    This will execute jobs on the farm, you need to wait until they are done. The output will be stored in `$DATA_DIR/$LUMI_DIR/XXX/$LUMI_OUT_DIR`.

    See also [Elastics tool](#elastics-tool) for details about the executed tool.

1. Merge outputs into summary files with again using the slice files.

    ```bash
    make_lumi_stats.sh -is $DATA_DIR/$LUMI_DIR/10M/$LUMI_SLICES_DIR/*.txt
    ```

    The scripts takes one or both of two possible arguments:
    * `-s` - calculate statistics for single days
    * `-i` - calculate integrals for all input days

    If the `-s` option is given and the slices are for days e.g. 10..13, then separate file for each day will be created.

    If the `-i` option is given and the slices are for days e.g. 10..13, then file for integral of days 10..13 will be created.

    Usually one wants to use both option to create files for each day (to analyze days separately) and the integral to analyze the whole beam time.
    Be aware that the order of arguments matter and the options `-i`, `-s` must be the first after the script name, like in the example.

    The output will be stored in `$DATA_DIR/$LUMI_DIR/XXX/$LUMI_SUM_DIR`.

    For the further luminosity calculations, this script will also copy extra scripts and files (helpers) into `$DATA_DIR/$LUMI_DIR/XXX/$LUMI_SUM_DIR/images` directory. See [Luminosity calculations](#luminosity-calculations) section for details. As the image scripts are intended to be modified by the users to adjust various parameters for luminosity calculations, whenever new summary is generated and the helper files are going to be copied, it will detect whether the target files were changed. If yes, it will show the differences and ask for confirmation. User must confirm whether to copy enw version or keep old one. This is to avoid overwriting of users versions. Again. it is user's responsibility to merge further changes if needed.

## Luminosity calculations

This step is performed in the `gnuplot` scripts together with drawing the plots. As the `vae` machines do not provide `gnuplot` this step is intended to be executed on a local machine. This is also favorable due to possibility to view the resulting plots.

1. One has to start with syncing the relevant files. On the lustre machine one can call:

    ```bash
    readlink -e $DATA_DIR/$LUMI_DIR/XXX/$LUMI_SUM_DIR
    ```

    Remember about replacing `XXX` with proper label. This will return the absolute path to the summary files, lets call it `ABS_SUMMARY`.

    Then, on your local machine sync the files using connection to the lustre:

    ```bash
    rsync -avz -e ssh lustre:ABS_SUMMARY XXX
    ```

    An example may look like this:
    > rsync -avz -e ssh lustre:/lustre/hades/user/rlalik/debian10/data/gen3/lumi/data/lumi/10M/summary 10M

    This will create local `10M` directory with the files.

1. Enter the `XXX/images` directory and call `hades_lumi_plot.sh`:

    ```bash
    ./hades_lumi_plot.sh
    ```

    It will detect each summary file (single or integral) ine directory `../` and plot respective image. The luminosity will be calculated by gnuplot. One can change parameters for calculations by editing the `hades_lumi_plot.gp` file. The `hades_lumi_plot.gp` file is created from `hades_lumi_plot.gp.tmpl` if it is not existig. User should modify only `hades_lumi_plot.gp`. This will protect the user changes from being destroyed by `rsync` or new calculations of summaries. If the `./hades_lumi_plot.sh` detects differences between `./hades_lumi_plot.gp` and `./hades_lumi_plot.gp.tmpl`, it will inform an user about them that user can merge chanegs manually. A such situation may happen when:
    1. User had modified `./hades_lumi_plot.gp`
    1. Upstream had modified `./hades_lumi_plot.gp.tmpl`

In order to configure parameters for luminosity calculations, one can edit the gnuplot script `hades_lumi_plot.gp`. The most relevant parameters are:

```gnuplot
# constants
target = 2e23           # target density
intensity = 7.5e7       # beam intensity
ds = 64                 # downscaling
eff = 0.15              # efficiency factor from elastics simulations
cs = 12.7e-27           # elastics cross-section
disk = 0.7              # disk storage
sim_exp = 1.0

# lumi factors
e_trig = 1              # trigger efficiency
e_duty = 0.5            # duty factor
e_LT = 0.7              # live time
e_paras = 0.9           # parasitics
e_fail = 0.9            # fail factor
```

The, the various luminosities are calculated using formulas:

```gnuplot
# Expected luminosity [cm^-2 s^-1] (Eq. 2)
L_th = intensity * target

L_goal = L_th * e_LT * e_trig * e_duty * e_paras * e_fail

# Integrated expected luminosity [cm^-2]
L_th_int(t) = L_th * period(t)

# Effective integrated luminosity (Eq. 3)
L_eff = L_th * e_trig * e_LT

# Integrated effective luminosity [cm^-2] (Eq. 11)
#L_eff_int(t) = pred(t) # L_eff * period(t) * e_duty * e_paras * e_fail
L_eff_int(t) = L_goal * period(t)

# experimental
L_exp(N) = N/eff * ds / cs * sim_exp
```

# Elastics tool

Elastics are being found using `hades_elastics_finder`. It is executed with following arguments:

```bash
hades_elastics_finder \
    -e number_of_events \
    -s first_event_offset \
    -o output_file.root
```

where:
* `number_of_events` is the slice size,
* `first_event_offset` is the first event to start analyzing data,
* `output_file.root` is the file where the output data will be stored.

The root file will have written the histograms and a file called `output_file.lumi` will be additionally created with text data. The `lumi` file has following format:

```
Timestamp   Duration   #Events   #PT2Events   Elastics_HH   Elastics_HF   Elastic_Sum
```
where:
* `Timestamp` is the unix time of the first analyzed event,
* `Duration` is the time length between the first and the last event,
* `#Events` is the number of all analyzed events (should equal to `number_of_events` or less if this was no more events in the slice),
* `#PT2Events` is the number of identified PT2 triggers,
* `Elastics_HH` is the number of identified HH elastic events,
* `Elastics_HF` is the number of identified HF elastic events,
* `Elastic_Sum` is the sum of identified HH and HF elastic events.

An example lumi file (slice):

```
      1644451673      2185        10000000          844402             348           27479           27827
```

has first event recorded on 10 February 2022 00:07:53, with duration of 2185 seconds, with 10M events and 844402 identified PT2 events. The tool found 348 HH elastics and 27479 HF elastics, and 27827 elastic all together.

The `Duration` is calculaetd based on the first and the last timestamp value and therefore may be incorrect - it is dependent on at which event the slice starts. It therefore should not be used to determine time-based values. To calculate number of elastics per events, the `Elastic_Sum` and `#PT2Events` should be used.

The user can replace the tool with its own. It can be configured by providing path to the tool in thr `$LUMI_TOOL` variable.

It is required however that the replacement tool accepts the same arguments, and returns the same output lumi file. Otherwise respective changes in `submit_lumi_slices.sh` and `batch_script_lumi_slices.sh` are also required.


# Contributing

See the [CONTRIBUTING](CONTRIBUTING.md) document.

# Licensing

<!--
Please go to https://choosealicense.com/licenses/ and choose a license that
fits your needs. The recommended license for a project of this type is the
GNU AGPLv3.
-->

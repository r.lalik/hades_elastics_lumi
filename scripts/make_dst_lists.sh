#!/bin/bash

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 2
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR

# dst files dir must be defined
[[ -n $DST_FILES_DIR ]] || exit 3

# dst lists dir must be defined
[[ -n $DST_LISTS_OUT_DIR ]] || exit 4
ABS_DST_LISTS_OUT_DIR=$ABS_DATA_DIR/$DST_LISTS_OUT_DIR
mkdir $ABS_DST_LISTS_OUT_DIR -p

for i in ${@}; do
    find $DST_FILES_DIR/$i/??/root -maxdepth 1 -iname "be??${i}*.hld_*.root" | sort -k 1.44,1.59 | tee $ABS_DST_LISTS_OUT_DIR/dst_list_$i.txt;
done

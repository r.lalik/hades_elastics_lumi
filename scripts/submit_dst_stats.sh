#!/bin/bash

FORCE_SUBMIT=0
DRYRUN=
ARRAY=
while getopts a:fn options; do
    case $options in
        a) ARRAY=$OPTARG;;
        f) FORCE_SUBMIT=1;;
        n) DRYRUN=echo;;
    esac
done
shift $((OPTIND-1))

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1


# data dir must be defined
[[ -n $DATA_DIR ]] || exit 2
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR/slurm_log -p

# profile must exists
make_abs "$HYDRA_PROFILE" "$WD" ABS_HYDRA_PROFILE
#[[ -f $ABS_HYDRA_PROFILE ]] || exit 2

# dst lists directory must exists
[[ -n $DST_LISTS_OUT_DIR ]] || exit 3
ABS_DST_LISTS_OUT_DIR=$ABS_DATA_DIR/$DST_LISTS_OUT_DIR
[[ -d $ABS_DST_LISTS_OUT_DIR ]] || exit 4

# lumi out dir must be defined
[[ -n $DST_STATS_OUT_DIR ]] || exit 5
ABS_DST_STATS_OUT_DIR=$ABS_DATA_DIR/$DST_STATS_OUT_DIR
mkdir $ABS_DST_STATS_OUT_DIR -p

# batch script must exists
make_abs "$DST_STATS_BATCH_SCRIPT" "$WD" ABS_DST_STATS_BATCH_SCRIPT
[[ -f $ABS_DST_STATS_BATCH_SCRIPT ]] || exit 6

# if variable is set, the file must exists
make_abs "$DST_STATS_TOOL" "$WD" ABS_DST_STATS_TOOL
[[ -n $ABS_DST_STATS_TOOL ]] || exit 7

inputs=(${@})

(( arr_len = ${#inputs[@]} ))

for day in ${inputs[@]}; do

    list=$ABS_DST_LISTS_OUT_DIR/dst_list_${day}.txt

    echo list=$list

    $DRYRUN sbatch -o $ABS_DATA_DIR/slurm_log/${LUMI_LOG_PREFIX}-%A_%a.log \
        --array=${ARR_DEF} \
        --time=180:00 --mem-per-cpu=1000mb -p main -J L_${day} \
        --export="PROFILE=$ABS_HYDRA_PROFILE,pattern=$list,tool=$ABS_DST_STATS_TOOL,odir=$ABS_DST_STATS_OUT_DIR" \
        -- $ABS_DST_STATS_BATCH_SCRIPT

done

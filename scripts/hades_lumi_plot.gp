#!/usr/bin/gnuplot --persist

#set style func linespoints

# set title "Total luminosity Feb22"

fontfamily="Arial"
TimeIntervalInSeconds = 3600*1
ct = time(0)

set bmargin 3

set timefmt "%s"
set xdata time

set yrange [ 0 : ]

set pixmap 1 'hadeslogo-big.png' at graph 0.125, 0.82 center width graph 0.15 front
# set pixmap 2 'HADES3D_AgAg_3840x2160_Labels.png' at screen 0.65, 0.10 width screen 0.30 back

set label 8 at graph 0.125, 0.65 center font "$fontfamily,20"
set label 8 "`cat description.txt`"


set ylabel "Integrated accepted luminosity [pb^{-1}]"
# set format x "%d/%m\n%H:%M"
set grid
set key enhanced opaque right bottom # at screen 0.75, 0.25


# first time stamp
ts_beg=1643670000 + TimeIntervalInSeconds
beam_beg = 1643932800 + TimeIntervalInSeconds

beam_break_1 = 1645507970
beam_break_2 = 1646061982

# trend plots specifications
ndays=7             # number of days per week :
use_xrange=1        # define x range or not

data_beg = beam_beg
data_now = ct + TimeIntervalInSeconds

data_beg=system(sprintf("head -1 %s | awk '{print $1}' ", ARG1))
data_now=system(sprintf("tail -1 %s | awk '{print $1}' ", ARG1))

# constants
target = 2e23           # target density
intensity = 7.5e7       # beam intensity
ds = 64                 # downscaling
eff = 0.15              # efficiency factor from elastics simulations
cs = 12.7e-27           # elastics cross-section
disk = 0.7              # disk storage
sim_exp = 1.0           # sim/exp correction factor

#exp_factor = 0.7 * 0.5  # magic factor (duty * something)

# lumi factors
e_trig = 1#0.09           # trigger efficiency
e_duty = 0.5            # duty factor
e_LT = 0.7              # live time
e_paras = 0.9           # parasitics
e_fail = 0.9            # fail factor

full_bt = 3600 * 8 * 84

# trend(N) = N/target/eff * ds * cs
pred(x) = (x < beam_break_1 ? (x-beam_beg) : x < beam_break_2 ? (beam_break_1 - beam_beg) : ( x - beam_beg - (beam_break_2 - beam_break_1) ) ) * intensity * target * exp_factor

# Time since the beam beginning [s], accounts the beam gap
period(x) = (x < beam_break_1 ? (x-beam_beg) : x < beam_break_2 ? (beam_break_1 - beam_beg) : ( x - beam_beg - (beam_break_2 - beam_break_1) ) )

# Expected luminosity [cm^-2 s^-1] (Eq. 2)
L_th = intensity * target

L_goal = L_th * e_LT * e_trig * e_duty * e_paras * e_fail

# Integrated expected luminosity [cm^-2]
L_th_int(t) = L_th * period(t)

# Effective integrated luminosity (Eq. 3)
L_eff = L_th * e_trig * e_LT

# Integrated effective luminosity [cm^-2] (Eq. 11)
#L_eff_int(t) = pred(t) # L_eff * period(t) * e_duty * e_paras * e_fail
L_eff_int(t) = L_goal * period(t)

# experimental
L_exp(N) = N/eff * ds / cs * sim_exp
L_expect(t) = 87.95 * t /eff * ds / cs

b_to_ub(x) = x*1e-24*1e-6
b_to_nb(x) = x*1e-24*1e-9
b_to_pb(x) = x*1e-24*1e-12

days_to_ts(n) = TimeIntervalInSeconds * 24 * n

set style fill transparent solid 0.4 noborder

print "L_th = ", b_to_ub(L_th), " ub^-2 s^-1"
print "L_th = ", b_to_pb(L_th), " pb^-2 s^-1"
print "L_goal = ", b_to_pb(L_goal) * full_bt, " pb^-2 s^-1"
print "L_eff = ", b_to_pb(L_eff), " pb^-2 s^-1"

# set terminal png transparent truecolor size 1600,800 enhanced font "$fontfamily,14"
set terminal png truecolor size 1600,800 enhanced font "$fontfamily,20"
set output ARG2

if (ARGC > 2) {
    set ylabel "Integrated accepted luminosity [nb^{-1}]"

    unset yrange
    plot \
        ARG1 u (last_time=($1+TimeIntervalInSeconds)):(last_lumi=b_to_nb(L_exp($5))) w lp pt 5 lc rgb "dark-green" lw 4 t "Data"

    set label 6 at first last_time, last_lumi*1.02 center font "$fontfamily,14" tc rgb "dark-green"
    set label 6 sprintf("{/:Bold %.2f pb^{-1}}", last_lumi)

    set output ARG2
    replot
} else {
    set ytics 1
    set mytics 4

    plot [ts_beg:] \
        ARG1 u (last_time=($1+TimeIntervalInSeconds)):(last_lumi=b_to_pb(L_exp($5))) w lp pt 5 lc rgb "dark-green" lw 4 t "Collected from data"

    set label 6 at first last_time, last_lumi*1.02 center font "$fontfamily,14" tc rgb "dark-green"
    set label 6 sprintf("{/:Bold %.2f pb^{-1}}", last_lumi)

    set output ARG2
    replot
}

unset label 6

unset ylabel
set title "Number of elastics [1/event] * 1000"
set format y "%0.1f"
#set ytics 2
set mytics 4

set grid mytics

unset pixmap 1
unset label 8


if (ARGC > 2) {
    ndays=ARG3
    nweeks=1
    use_xrange=0
}
else {
    nweeks = ceil(1.0*(data_now - data_beg)/days_to_ts(ndays))
}

print "Read ", ARGC, " data from: ", ARG1
print "date beg: ", data_beg, " date now: ", data_now
print "mdays = ", ndays, " nweeks = ", nweeks, " float weeks = ", 1.0*(data_now - data_beg)/days_to_ts(ndays)

print data_beg, " ", data_now, " ", nweeks

set terminal png truecolor size 1800,100+288*nweeks enhanced font "$fontfamily,20"
set output "trend_".ARG2

set multiplot layout nweeks, 1

unset yrange
#set ytics 1

do for [i=0:nweeks-1] {
    print "Display week: ", i

    if (use_xrange==1) {
        print "Set range: ", data_beg+days_to_ts(i*ndays), " to ", data_beg+days_to_ts((i+1)*ndays)
        xsta=data_beg+days_to_ts(i*ndays)
        xsto=data_beg+days_to_ts((i+1)*ndays)
        set xrange [xsta:xsto]
    }
    plot ARG1 u ($1+TimeIntervalInSeconds):($7/$8 * 1000) w lp pt 5 lc rgb "dark-green" lw 4 t ""
    unset title
}

unset multiplot

set terminal png truecolor size 800,800 enhanced font "$fontfamily,20"
set output "hist_".ARG2

reset
n=100 #number of intervals
max=5. #max value
min=0. #min value
width=(max-min)/n #interval width

# function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0
set xrange [min:max]
set yrange [0:]

# to put an empty boundary around the data inside an autoscaled graph
set offset graph 0.05,0.05,0.05,0.0
set xtics min,(max-min)/5,max
set boxwidth width*0.9
set style fill solid 0.5 #fillstyle
set tics out nomirror
set xlabel "Elastics * 1000 per 1 event"
set ylabel "Frequency"

tot_events = 38921336068.0
tot_elas = 108904205.0
tot_avg = (tot_elas/38921336068.0 * 1000)
set label 1 at screen 0.2, 0.50 sprintf("Total events: %.0f", tot_events)
set label 2 at screen 0.2, 0.45 sprintf("Total elast.: %.0f", tot_elas)
set label 3 at screen 0.2, 0.40 sprintf("Average     : %.2f", tot_avg)

#count and plot
plot ARG1 u (hist($7/$8*1000,width)):(1.0) smooth freq w boxes lc rgb"dark-green" notitle

#!/bin/bash

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY ${SLURM_ARRAY_TASK_ID}"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. $PROFILE

file=$pattern
filebn=$(basename $file .hld)
file=$(readlink -e $file)
day=${filebn:9:3}

odir=$(readlink -e $odir)


echo inputfile=$file
echo outputdir=$odir
echo toolname=$tool
echo extraargs=$extra

echo chunk=$chunk
echo events=$events
echo length=$(( chunk * (SLURM_ARRAY_TASK_ID - 1) ))

mkdir -p $odir

crashfile=$odir/crash_await_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt

# mark that file is submitted, may be killed
echo $day $events $chunk ${SLURM_ARRAY_TASK_ID} > $crashfile

ofname=lumi_stat_${day}_$(printf "%04d" ${SLURM_ARRAY_TASK_ID})

echo $tool $file \
    -e $chunk \
    -o $odir/$ofname.root \
    -s $(( chunk * (SLURM_ARRAY_TASK_ID - 1) )) \
    --exp \
    $extraargs

time $tool $file \
    -e $chunk \
    -o $odir/$ofname.root \
    -s $(( chunk * (SLURM_ARRAY_TASK_ID - 1) )) \
    --exp \
    $extraargs

res=$?

rm $crashfile

echo result=$res

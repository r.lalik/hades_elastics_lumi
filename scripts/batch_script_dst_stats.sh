#!/bin/bash

date

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY ${SLURM_ARRAY_TASK_ID}"
    file=$(sed -n $((${SLURM_ARRAY_TASK_ID}))p ${pattern})
else
    echo "NO ARRAY"
    file=$pattern
fi

. $PROFILE

file=$pattern
filebn=$(basename $file .hld)
file=$(readlink -e $file)
day=${filebn:9:3}

odir=$(readlink -e $odir)


echo inputfile=$file
echo outputdir=$odir
echo toolname=$tool
echo extraargs=$extra

mkdir -p $odir

crashfile=$odir/crash_await_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt

# mark that file is submitted, may be killed
echo $day > $crashfile

stat_file=dst_stat_${day}.txt

echo $tool $file \
    -o $odir/$stat_file \
    $extraargs

time $tool $file \
    -o $odir/$stat_file \
    $extraargs

res=$?

rm $crashfile

echo result=$res

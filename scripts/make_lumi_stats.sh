#!/bin/bash

INTEGRATE=0
SINGLES=0
while getopts is options; do
    case $options in
        i) INTEGRATE=1;;
        s) SINGLES=1;;
    esac
done
shift $((OPTIND-1))

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 2
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR/slurm_log -p

# lumi out dir must be defined
[[ -n $LUMI_DIR ]] || exit 3
[[ -n $LUMI_OUT_DIR ]] || exit 4
[[ -n $LUMI_SUM_DIR ]] || exit 5

integrated_list=()
first_day=
last_day=

echo "REQUESTED ACTIONS"
echo "* single days   : ${SINGLES}"
echo "* days integral : ${INTEGRATE}"
echo

for slice_file in ${@}; do
    while IFS= read -r line; do
        echo "* Input: $slice_file -> $line"
        args=($line)

        # parse line
        day=${args[0]}
        total=${args[1]}
        chunk=${args[2]}
        slot=${args[3]}
    done < $slice_file

    fmtstr=$(numfmt --to=si $chunk)
    ABS_LUMI_OUT_DIR=$ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_OUT_DIR
    ABS_LUMI_SUM_DIR=$ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR
    mkdir -p "$ABS_LUMI_SUM_DIR"

    if [ $SINGLES -eq 1 ]; then
        echo ./hades_lumi_merger.py $ABS_LUMI_OUT_DIR/lumi_stat_${day}\*.lumi
        exec ./hades_lumi_merger.py $ABS_LUMI_OUT_DIR/lumi_stat_${day}\*.lumi | tee $ABS_LUMI_SUM_DIR/lumi_single_${day}.dat
    fi

    if [ $INTEGRATE -eq 1 ]; then
        integrated_list+=($ABS_LUMI_OUT_DIR/lumi_stat_${day}\*.lumi)

        [[ -z $first_day ]] && first_day=$day
        last_day=$day
        echo "Integrated list of days from $first_day to $last_day contain ${#integrated_list[@]} patterns"
    fi
done

if [[ $INTEGRATE -eq 1 && -d $ABS_DATA_DIR/$LUMI_DIR/$fmtstr ]]; then
    echo ./hades_lumi_merger.py "${integrated_list[@]}"
    exec ./hades_lumi_merger.py "${integrated_list[@]}" | tee $ABS_LUMI_SUM_DIR/lumi_integrated_${first_day}_${last_day}.dat
fi

echo
echo "REQUESTED ACTIONS"
echo "* single days   : ${SINGLES}"
echo "* days integral : ${INTEGRATE}"

# Based on generated with https://zzzcode.ai/code-generator?id=9f1cd761-8d12-431d-ac0b-459150f20b97
function copy_with_confirmation() {
    cmp -s "$1" "$2"
    cmp_res=$?

    if [[ $cmp_res -eq 2 ]]; then
        cp -v "$1" "$2"
    elif [[ $cmp_res -eq 1 ]]; then
        echo "The target file differs from source:"
        diff --color "$1" "$2"
        read -p "Do you want to copy the file? (y/n): " answer
        if [ "$answer" == "y" ]; then
            cp -v "$1" "$2"
        fi
    else
        echo "Image creation file $2 is up-to-date."
    fi
}

if [[ -d $ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR ]]; then
    mkdir $ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR/images -p
    copy_with_confirmation hades_lumi_plot.gp $ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR/images/hades_lumi_plot.gp.tmpl
    copy_with_confirmation hades_lumi_plot.sh $ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR/images/hades_lumi_plot.sh
    copy_with_confirmation hadeslogo-big.png $ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SUM_DIR/images/hadeslogo-big.png
fi

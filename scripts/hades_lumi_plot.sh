#!/bin/bash

LUMIDIR=../

LOGFILE=plotting.log
DESCFILE=description.txt

truncate -s0 $LOGFILE

[ ! -f hades_lumi_plot.gp ] && cp -v hades_lumi_plot.gp.tmpl hades_lumi_plot.gp

cmp -s hades_lumi_plot.gp.tmpl hades_lumi_plot.gp
cmp_res=$?

if [[ ! $cmp_res -eq 0 ]]; then
    echo "The files 'hades_lumi_plot.gp.tmpl' and 'hades_lumi_plot.gp' differ."
    diff --color -Naur hades_lumi_plot.gp.tmpl hades_lumi_plot.gp
    echo "Please merge changes manually if required and restart this script."
    echo
fi

[ ! -f $DESCFILE ] && \
    echo -e "Update $DESCFILE to change this text\nand describe your data. See\nhttp://gnuplot.info/demo/enhanced_utf8.html\nfor examples." | tee $DESCFILE


for summary_file in $LUMIDIR/lumi_*.dat; do
    echo "Checking $(readlink -e $summary_file) for plotting."

    png_name=$(basename $summary_file .dat).png

    if [[ ! -f $png_name || $summary_file -nt $png_name || hades_lumi_plot.gp -nt $png_name || $DESCFILE -nt hades_lumi_plot.gp ]]; then

        if [[ $png_name == *_single_* ]]; then
            gnuplot -c hades_lumi_plot.gp $summary_file $png_name 1 1>>$LOGFILE 2>&1
        else
            gnuplot -c hades_lumi_plot.gp $summary_file $png_name 1>>$LOGFILE 2>&1
        fi
    fi
done

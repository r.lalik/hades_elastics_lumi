#!/bin/bash

FORCE_SUBMIT=0
DRYRUN=
ARRAY=
while getopts a:fn options; do
    case $options in
        a) ARRAY=$OPTARG;;
        f) FORCE_SUBMIT=1;;
        n) DRYRUN=echo;;
    esac
done
shift $((OPTIND-1))

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1


# data dir must be defined
[[ -n $DATA_DIR ]] || exit 2
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR/slurm_log -p

# profile must exists
make_abs "$HYDRA_PROFILE" "$WD" ABS_HYDRA_PROFILE
#[[ -f $ABS_HYDRA_PROFILE ]] || exit 2

# dst lists directory must exists
[[ -n $DST_LISTS_OUT_DIR ]] || exit 3
ABS_DST_LISTS_OUT_DIR=$ABS_DATA_DIR/$DST_LISTS_OUT_DIR
[[ -d $ABS_DST_LISTS_OUT_DIR ]] || exit 4

# lumi out dir must be defined
[[ -n $LUMI_DIR ]] || exit 5
[[ -n $LUMI_OUT_DIR ]] || exit 6

# batch script must exists
make_abs "$LUMI_BATCH_SCRIPT" "$WD" ABS_LUMI_BATCH_SCRIPT
[[ -f $ABS_LUMI_BATCH_SCRIPT ]] || exit 7

# if variable is set, the file must exists
make_abs "$LUMI_TOOL" "$WD" ABS_LUMI_TOOL
[[ -n $ABS_LUMI_TOOL ]] || exit 8

for f in ${@}; do

    while IFS= read -r line; do
        echo "* Input: $f -> $line"
        args=($line)

        # parse line
        day=${args[0]}
        total=${args[1]}
        chunk=${args[2]}
        slot=${args[3]}
    done < $f

    fmtstr=$(numfmt --to=si $chunk)
    ABS_LUMI_OUT_DIR=$ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_OUT_DIR
    mkdir $ABS_LUMI_OUT_DIR -p

    (( arr_len = (total + chunk - 1)/chunk ))

    echo " - total=$total"
    echo " - chunk=$chunk"
    echo " - slot=$slot"
    echo " - arr len=$arr_len"

    [[ -n "${ARRAY}" ]] && ARR_DEF=$ARRAY || [[ -n "${slot}" ]] && ARR_DEF=$slot || ARR_DEF=1-${arr_len}

    echo " - arr def=$ARR_DEF"

    list=$ABS_DST_LISTS_OUT_DIR/dst_list_${day}.txt

    echo list=$list

    $DRYRUN sbatch -o $ABS_DATA_DIR/slurm_log/${LUMI_LOG_PREFIX}-%A_%a.log \
        --array=${ARR_DEF} \
        --time=180:00 --mem-per-cpu=1000mb -p main -J L_${day} \
        --export="PROFILE=$ABS_HYDRA_PROFILE,pattern=$list,events=$total,chunk=$chunk,tool=$ABS_LUMI_TOOL,odir=$ABS_LUMI_OUT_DIR" \
        -- $ABS_LUMI_BATCH_SCRIPT

done

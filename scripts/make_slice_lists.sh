#!/bin/bash

WD=$(dirname $(readlink -e ${BASH_SOURCE[0]}))

function make_abs()
{
    local __returnval=$3
    if [[ "$1" = /* ]]; then
        local myresult=$1
    else
        local myresult=$(readlink -e $2/$1)
    fi

    eval $__returnval="'$myresult'"
}

fmtstr=$(numfmt --to=si $1)

[ -f $WD/config.sh ] && . $WD/config.sh || exit 1

# data dir must be defined
[[ -n $DATA_DIR ]] || exit 2
make_abs "$DATA_DIR" "$WD" ABS_DATA_DIR
mkdir $ABS_DATA_DIR/slurm_log -p

# dst lists directory must exists
[[ -n $DST_STATS_OUT_DIR ]] || exit 3
ABS_DST_STATS_OUT_DIR=$ABS_DATA_DIR/$DST_STATS_OUT_DIR
[[ -d $ABS_DST_STATS_OUT_DIR ]] || exit 4

# lumi out dir must be defined
[[ -n $LUMI_DIR ]] || exit 5
[[ -n $LUMI_SLICES_DIR ]] || exit 6
ABS_LUMI_SLICES_DIR=$ABS_DATA_DIR/$LUMI_DIR/$fmtstr/$LUMI_SLICES_DIR
mkdir $ABS_LUMI_SLICES_DIR -p

inputs=(${@:2})

for day in ${inputs[@]}; do
    stat_file="$ABS_DST_STATS_OUT_DIR/dst_stat_${day}.txt"

    echo "$day $(cat $stat_file) $1" | tee $ABS_LUMI_SLICES_DIR/lumi_slice_input_$day.txt
done

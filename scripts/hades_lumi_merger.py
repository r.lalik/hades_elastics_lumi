#!/usr/bin/python3

import argparse
import glob
import sys

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--foo', help='foo help')

    args, opts = parser.parse_known_args()

    file_duration = {}
    n_evts = {}
    n_evtspt2 = {}
    n_elas_hh = {}
    n_elas_hf = {}
    n_elas_sum = {}

    counter = 0
    accepted = 0
    skipped = 0

#    if len(opts) > 1:
#        print("Only one directory allowed")
#        sys.exit(1)

    for file_filter in opts: #[0] # + "/*.lumi" if len(opts) == 1 else "*.lumi"
        inputs = sorted(glob.glob(file_filter))

        for f in inputs:
            counter = counter + 1
            print(f"({counter}) Read {f} ({counter})", file=sys.stderr, end='\r')

            with open(f) as file:
                line = file.readline()
                sl = [float(x) for x in line.split()[0:7]]
                if len(sl) != 7:
                    continue

                if sl[0] == 0:
                    continue
                k=sl[0]  # k - key, a timestamp
                d=sl[1]
                e=sl[2]
                ept2=sl[3]
                ehh=sl[4]
                ehf=sl[5]
                esum=sl[6]

                if not k in n_evts:
                    file_duration[k] = 0
                    n_evts[k] = 0
                    n_evtspt2[k] = 0
                    n_elas_hh[k] = 0
                    n_elas_hf[k] = 0
                    n_elas_sum[k] = 0

                if d > file_duration[k]:
                    file_duration[k] = d
                n_evts[k] = n_evts[k] + e
                n_evtspt2[k] = n_evtspt2[k] + ept2
                n_elas_hh[k] = n_elas_hh[k] + ehh
                n_elas_hf[k] = n_elas_hf[k] + ehf
                n_elas_sum[k] = n_elas_sum[k] + esum

    print(f"\n", file=sys.stderr)

    start_t = 0
    last_t = 0
    # accumulated values
    acc_evts = 0   # no. events
    acc_evtspt2 = 0   # no. elastics
    tot_elas_hh = 0   # total duration
    tot_elas_hf = 0 # total lumi hh
    tot_elas_sum = 0

    prev_elas_sum = 0
    # previous values
    prev_t = 0
    n = 0
    for k, v in n_evts.items():
        n = n + 1

        prev_t = k

        acc_evts = acc_evts + n_evts[k]
        acc_evtspt2 = acc_evtspt2 + n_evtspt2[k]
        tot_elas_hh = tot_elas_hh + n_elas_hh[k]
        tot_elas_hf = tot_elas_hf + n_elas_hf[k]
        tot_elas_sum = tot_elas_sum + n_elas_sum[k]

        last_t = k

        print(f"{prev_t}  {acc_evts}  {acc_evtspt2}  {tot_elas_hh}  {tot_elas_hf}  {tot_elas_sum}  {tot_elas_sum-prev_elas_sum}  {n_evts[k]}")

        prev_elas_sum = tot_elas_sum
